package com.example.paolobabasa.pokedex;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Controller;


/**
 * Created by paolobabasa on 08/07/2018.
 */

public abstract class BaseController extends ButterKnifeController {

    protected BaseController() {

    }
    protected BaseController(Bundle args) {
        super(args);
    }

    @Override
    protected View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return  null;
    }


    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);
    }

    @Override
    protected void onDestroyView(@NonNull View view) {
        super.onDestroyView(view);
    }



}
