package com.example.paolobabasa.pokedex.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.paolobabasa.pokedex.App;
import com.example.paolobabasa.pokedex.BaseController;
import com.example.paolobabasa.pokedex.R;
import com.example.paolobabasa.pokedex.data.model.PokemonList;

import butterknife.BindView;

/**
 * Created by paolobabasa on 08/07/2018.
 */

public class ListController extends BaseController {
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    private ListPresenter listPresenter;
    private ListAdapter listAdapter;
    private LinearLayoutManager linearLayoutManager;
    @Override
    protected View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        listPresenter = new ListPresenter(App.getPokemonService(), this);
        return inflater.inflate(R.layout.controller_list, container, false);

    }

    @Override
    protected void onViewBound(@NonNull View view) {
        super.onViewBound(view);
        listPresenter.Start();
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvList.setLayoutManager(linearLayoutManager);
        listAdapter = new ListAdapter();
        rvList.setAdapter(listAdapter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        listPresenter.Destroy();
    }


    public void setPokemonList(PokemonList pokemonList) {
        listAdapter.setPokemonLists(pokemonList.getResults());
    }

}
