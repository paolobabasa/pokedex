package com.example.paolobabasa.pokedex.list;

import com.example.paolobabasa.pokedex.Constants;
import com.example.paolobabasa.pokedex.api.PokemonService;
import com.example.paolobabasa.pokedex.data.model.Pokemon;
import com.example.paolobabasa.pokedex.data.model.PokemonList;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by paolobabasa on 08/07/2018.
 */

public class ListHelper {
    private PokemonService pokemonService;
    public ListHelper(PokemonService pokemonService) {
        this.pokemonService    = pokemonService;
    }

    Single<PokemonList> getPokemonList() {
        return pokemonService.pokemonList();
    }
}
