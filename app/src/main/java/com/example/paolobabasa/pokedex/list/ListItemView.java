package com.example.paolobabasa.pokedex.list;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.paolobabasa.pokedex.R;
import com.example.paolobabasa.pokedex.data.model.Pokemon;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by paolobabasa on 08/07/2018.
 */

public class ListItemView  extends LinearLayout {
    @BindView(R.id.tv_pokemon_name)
    TextView pokemonName;
    public ListItemView(Context context) {
        super(context);
    }

    public ListItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ListItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void bindTo(Pokemon pokemon) {
        pokemonName.setText(pokemon.getName());
    }
}
