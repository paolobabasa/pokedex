package com.example.paolobabasa.pokedex.list;

import android.util.Log;
import android.widget.Toast;

import com.example.paolobabasa.pokedex.App;
import com.example.paolobabasa.pokedex.BasePresenter;
import com.example.paolobabasa.pokedex.api.PokemonService;
import com.example.paolobabasa.pokedex.data.model.PokemonList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by mynddynamicios on 10/07/2018.
 */

public class ListPresenter implements BasePresenter {
    private final PokemonService pokemonService;
    private final ListController listController;
    public ListPresenter(PokemonService pokemonService, ListController listController) {
        this.pokemonService = pokemonService;
        this.listController = listController;
    }

    @Override
    public void Start() {
        getListPokemon();
    }

    @Override
    public void Destroy() {

    }


    public void getListPokemon() {
        ListHelper listHelper = new ListHelper(App.getPokemonService());
        listHelper.getPokemonList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<PokemonList>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(PokemonList pokemonList) {
                        listController.setPokemonList(pokemonList);
                        //Toast.makeText(listController.getActivity(), pokemonList.getResults().get(0).getName(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error get list pokemon", e.getMessage());
                    }
                });
    }


}
